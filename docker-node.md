# 使用Docker跑node程序

要求使用node的版本为8以上

# node 项目

项目的目录

```
|-- server.js
|-- package.json
|-- package-lock.json
|-- Dockerfile
|-- .dockerignore
```

```javascript
// server.js
const express = require('express');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();

app.get('/', (req, res) => {
    res.send('Hello Docker!');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
```

package.json 文件的`scripts`命令

```json
// package.json
...
"scripts": {
    "start": "node server.js"
}
...
```


Dockerfile 文件
```
# Dockerfile
FROM node:8

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where avaliable (name@5+)
COPY package*.json ./

RUM npm install
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 8080
CMD ["npm", "start"]

```

docker生成镜像忽略的文件或文件夹

```
# .dockerignore
node_modules
node-debug*.log
```

## 生成项目镜像

-t 为项目镜像做一个标记，可以通过`docker images`找到该项目镜像

```shell
# 生成镜像
docker build -t <your username>/node-web-app .

# 查看生成的镜像
docker images

# 运行镜像
# -p 是镜像的端口映射系统的端口
docker run <your name>/node-web-app -p 49166:8080

# 查看容器id container ID
docker ps

# 查看容器的日志
docker logs <container id>

# 进入容器，使用exec
docker exec -it <container id> /bin/bash
```